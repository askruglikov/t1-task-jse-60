package ru.t1.kruglikov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.kruglikov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    protected AbstractUserOwnedDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}