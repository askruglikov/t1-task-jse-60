package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;

import java.util.List;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll(
            @NotNull String userId,
            @NotNull ProjectSort sort
    );

    @Nullable
    List<ProjectDTO> findAll(
            @NotNull ProjectSort sort
    );

}

