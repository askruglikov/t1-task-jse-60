package ru.t1.kruglikov.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.model.Task;

@NoArgsConstructor
public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
