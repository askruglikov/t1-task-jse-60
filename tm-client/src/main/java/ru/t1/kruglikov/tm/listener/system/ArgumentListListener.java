package ru.t1.kruglikov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.listener.AbstractListener;
import ru.t1.kruglikov.tm.event.ConsoleEvent;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show argument list.";

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@Nullable final AbstractListener abstractListener : listeners) {
            if (abstractListener == null) continue;
            @Nullable final String argument = abstractListener.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
